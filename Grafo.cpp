#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <set>

// BFS, MST, DFS

using namespace std;

class aresta{
    private:
    string nome;
    int id;
    int peso;

    public:
    aresta(string nome_, int id_, int peso_){
        nome = nome_;
        id = id_;
        peso = peso_;

    }
    void set_id(int id_){id = id_;}
    int get_id(){return id;}
    string get_nome(){return nome;}
    int get_peso(){return peso;}
};
class vertice{
    private:
    string nome;
    int cor;
    int distancia;
    int id;
    int tFinalizacao;
    int tDescoberta;
    bool isArticulacao;
    vertice* path;
    list<aresta*>* arestas;

    public:
    vertice(string nome_, int id_){
        nome = nome_;
        arestas = NULL;
        id = id_;
        cor = 0;
        isArticulacao = true;
        path = NULL;
        tFinalizacao = 0;
        tDescoberta = 0;
        distancia = -1;
        }
    void set_arestas(list<aresta*>* arestas_){arestas = arestas_;}
    list<aresta*>* get_arestas(){return arestas;}
    string get_nome(){return nome;}
    void set_nome(string nome_){nome = nome_;}
    void set_cor(int i){cor = i;}
    int get_cor(){return cor;}
    void set_distancia(int i){distancia = i;}
    int get_distancia(){return distancia;}
    int get_id(){return id;}
    void set_tDescoberta(int i){tDescoberta = i;}
    int get_tDescoberta(){return tDescoberta;}
    void set_tFinalizacao(int i){tFinalizacao = i;}
    int get_tFinalizacao(){return tFinalizacao;}
    void set_path(vertice* i){path = i;}
    vertice* get_path(){return path;}
    void set_isArticulacao(bool i){isArticulacao = i;}
    bool get_isArticulacao(){return isArticulacao;}
    };
class noT{
    private:
    int origem;
    int destino;
    int peso;
    public:
    noT(int o, int d, int p){
        origem = o;
        destino = d;
        peso = p;
    }
    int get_Origem(){return origem;}
    int get_Destino(){return destino;}
    int get_Peso(){return peso;}
};

class Comparator
{
public:
	bool operator()(noT* n1, noT* n2){return ((n1->get_Peso())>(n2->get_Peso()));}
};

class grafo{
    private:
    vector<vertice*> vertices;

    public:
    int nivel_max;
    int insereVertice(string nome){
        for(int i = 0; i < vertices.size(); i++){
            if((vertices[i])->get_nome().compare(nome)==0)
                return i;
        }

        vertice* novo_vertice = new vertice(nome,vertices.size()+1);
        vertices.push_back(novo_vertice);


        list<aresta*>* nova_lista = new list<aresta*>;
        vertices[vertices.size()-1]->set_arestas(nova_lista);
        return vertices.size()-1;
        };

    void insereAresta(string nome1, string nome2, int peso){
        int id1 = insereVertice(nome1);
        int id2 = insereVertice(nome2);

        aresta* v1 = new aresta(nome1, id1, peso);
        aresta* v2 = new aresta(nome2, id2, peso);

        vertices[id1]->get_arestas()->push_back(v2);
        vertices[id2]->get_arestas()->push_back(v1);
    };

    void printa(){
    //vertice * novo  = vertices[]->get_path();
    //  cout << novo->get_id();

        for(int i=0;i<vertices.size();i++){
      //      int novo  = vertices[0]->get_path()->get_id();
            list<aresta*>::iterator it;
            cout<<i <<vertices[i]->get_nome()<< "::" << vertices[i]->get_tDescoberta() << "::" << vertices[i]->get_tFinalizacao()<< "::" << vertices[i]->get_path()->get_id()<< "::" <<vertices[i]->get_isArticulacao()<< endl  ;

     //       for(it = vertices[i]->get_arestas()->begin(); it!=vertices[i]->get_arestas()->end(); it++)
      //          cout<<(*it)->get_nome()<<"->"<<endl;
        }
    };
    void Amigos(string Nome_){
        for(int i = 0; i < vertices.size(); i++){
            if(vertices[i]->get_nome().compare(Nome_)==0){
                 BFS(i);
                 cout << "Manolo: " << vertices[i]->get_nome()<<endl;
                  for(int k = 1; k < nivel_max + 1; k++){
                         cout << "Distancia "<<k <<endl;
                         for(int j = 0; j < vertices.size(); j++){
                            if(vertices[j]->get_distancia() == k){
                            cout << vertices[j]->get_nome()<<endl;
                            }
                    }
                     cout<<endl;
                 }
                 return;
                }
        }
        cout << "Nome nao existe";
    };
    void BFS(int id){
        for(int i = 0; i < vertices.size(); i++){
            vertices[i]->set_cor(0);
            vertices[i]->set_distancia(-1);
        }
        vertice* u;
        nivel_max = 0;
        vertices[id]->set_cor(1);
        vertices[id]->set_distancia(0);
        list<vertice*> Fila;
        Fila.push_back(vertices[id]);
        list<aresta*>::iterator it;
        while(!Fila.empty()){
            u = Fila.front();
            Fila.pop_front();
            for(it = u->get_arestas()->begin(); it!=u->get_arestas()->end(); it++){
                if(vertices[(*it)->get_id()]->get_cor() == 0){
                    vertices[(*it)->get_id()]->set_cor(1);
                    vertices[(*it)->get_id()]->set_distancia(u->get_distancia()+1);
                    if(u->get_distancia()+1 > nivel_max){nivel_max = u->get_distancia()+1;}
                    Fila.push_back(vertices[(*it)->get_id()]);
                }
            }
            u->set_cor(2);
        }
    }
    void pontosArticulacao(){
        for(int i = 0; i < vertices.size(); i++){
            vertices[i]->set_cor(0);
            vertice *vazio = new vertice("",0);
            vertices[i]->set_path(vazio);
        }
        int tempo = 0;
        for(int i = 0; i < vertices.size(); i++){
            if(vertices[i]->get_cor() == 0)
            DFSVisit( i , -1, tempo);
        }
        int j = 0;
        for(int i = 0; i< vertices.size(); i++){
            if(vertices[i]->get_isArticulacao() == true){
                if(vertices[i]->get_tFinalizacao() != vertices[i]->get_tDescoberta()+1){
                    cout << vertices[i]->get_nome()<<" � um ponto de articula��o entre grupo de usu�rios"<<endl;
                    j=1;}
        }}
        if(j!=1)
        cout<<"O grafo carregado n�o possui pontos de articula��o entre grupos de usu�rios"<<endl;
    }
    void DFSVisit(int id,int id_ant, int &tempo){
        tempo++;
        vertice *u = vertices[id];
        u->set_tDescoberta(tempo);
        u->set_cor(1);
        list<aresta*>::iterator it;
        for(it = u->get_arestas()->begin(); it!=u->get_arestas()->end(); it++){
           // cout << id << "::" << (*it)->get_id()<<endl;
            if(vertices[(*it)->get_id()]->get_cor() == 0){
                vertices[(*it)->get_id()]->set_path(u);
                //cout<<"BRANCO"<<endl;
                DFSVisit((*it)->get_id(),id,tempo);

            }
            else if(vertices[(*it)->get_id()]->get_cor() == 1){
                //cout<<"CINZA"<<endl;
                if((*it)->get_id() != id_ant){
                    vertice *auxiliar = vertices[id]->get_path();
                    while (auxiliar!=NULL && auxiliar->get_id() != (*it)->get_id()){
                        auxiliar->set_isArticulacao(false);
                        auxiliar = auxiliar ->get_path();
                    }
                }
            }

        }
        u->set_cor(2);

        tempo++;
        u->set_tFinalizacao(tempo);
    }
void MST(){
        vector<noT*> MST;
		multiset<noT*,Comparator> arvore;
		for(int i=0; i<vertices.size(); i++){
			if(!vertices[i]->get_arestas()->empty()){
				list <aresta*>::iterator it;
            for(it=vertices[i]->get_arestas()->begin(); it!=vertices[i]->get_arestas()->end(); it++){
                noT *aux = new noT(i,(*it)->get_id( ),(*it)->get_peso());
				int k=0;
				multiset<noT*>::iterator ij;
				for(ij=arvore.begin(); ij!=arvore.end(); ij++){
                    if((aux->get_Origem()!=(*ij)->get_Destino())||(aux->get_Destino()!=(*ij)->get_Origem())){
							k++;
                    }
                }
                if(k==(arvore.size())){arvore.insert(aux);}
            }
			}
		}
		while((MST.size()<arvore.size())&&(MST.size()<(vertices.size()-1)))	{
			noT* aux = *(arvore.begin());
			arvore.erase(arvore.begin());
			int k=0;
			for(int i=0; i<MST.size(); i++){
				if(aux->get_Destino()==MST[i]->get_Origem()){
					k++;
					break;
				}
			}
			if(k==0){MST.push_back(aux);}
		}
		cout<<"MST:"<<endl;
		for(int i=0; i<MST.size(); i++){
			cout<<vertices[MST[i]->get_Origem()]->get_nome()<<"  "<<vertices[MST[i]->get_Destino()]->get_nome()<<" "<<MST[i]->get_Peso()<<endl;
		}
	}
};
int main()
{   grafo novo;
    ifstream arquivo("grafo-rede-social.txt");
    string nome1;
    string nome2;
    int peso;
     while(!arquivo.eof()){
         arquivo >> nome1;
         arquivo >> nome2;
         arquivo >> peso;

         novo.insereAresta(nome1,nome2,peso);
    }
    cout<<"NIVEIS DE AMIZADE: "<<endl;
    novo.Amigos("CONE");
    novo.MST();
    novo.pontosArticulacao();

}
