#include <iostream>
#include <fstream>
#include <stdexcept>
#include<ctime>
#include<cstdlib>
#include<string>
#include<ctime>
#include<cstring>

using namespace std;
typedef long long PageID;
class Page
{
private:
    int size;
    PageID id;
    char * data;
public:
    Page(int nSize)
    {
        size = nSize;
        data = new char[size];
    };
    ~Page()
    {
        delete[] data;
    };
    int getSize()
    {
        return size;
    };
    void setId(PageID nId)
    {
        id = nId;
    };
    PageID getId()
    {
        return id;
    };
    char * getData()
    {
        return data;
    };
};
template < class KeyType, int OBJECT_SIZE >
class BNode
{

private:
    int maxEntries;
    int *numEntries;
    PageID * pointers;
    KeyType * keys;
    char * objects;

public:
    BNode(Page * page, bool isNew)
    {
        int entrySize = sizeof(PageID) + sizeof(KeyType) + OBJECT_SIZE;
        maxEntries = (page->getSize() - sizeof(PageID)) / entrySize;
        numEntries = (int *) page->getData();
        pointers = (PageID *) (page->getData() + sizeof(int));
        keys = (KeyType *) ( page->getData() + sizeof(int) + ((maxEntries+1) * sizeof(PageID)) );
        objects = (char *) ( page->getData() + sizeof(int) + ((maxEntries+1) * sizeof(PageID)) + (maxEntries * sizeof(KeyType)) );
        if (isNew)
        {
            *numEntries = 0;
            pointers[0] = 0;
        }
    }
    int getMaxEntries()
    {
        return maxEntries;
    }
    void setPointer(int i, PageID pageId)
    {
        if ((i < 0) || (i > maxEntries))
            throw out_of_range("Index out of range");
        else
            pointers[i] = pageId;
    }
    PageID getPointer(int i)
    {
        if ((i < 0) || (i > maxEntries))
            throw out_of_range("Index out of range");
        else
            return pointers[i];
    }
    void setKey(int i, KeyType key)
    {
        if ((i < 0) || (i >= maxEntries))
            throw out_of_range("Index out of range");
        else
            keys[i] = key;
    }
    KeyType getKey(int i)
    {
        if ((i < 0) || (i >= maxEntries))
            throw out_of_range("Index out of range");
        else
            return keys[i];
    }
    void setObject(int i, char * obj)
    {
        if ((i < 0) || (i > *numEntries + 1))
            throw out_of_range("Index out of range");
        else
            memcpy(&objects[i * OBJECT_SIZE], obj, OBJECT_SIZE);
    }
    char * getObject(int i)
    {
        if ((i < 0) || (i >= maxEntries))
            throw out_of_range("Index out of range");
        else{
            char * outObj = new char[OBJECT_SIZE];
            memcpy(outObj, &objects[i * OBJECT_SIZE], OBJECT_SIZE);
            return outObj;
        }
    }
    void setNumEntries(int nEntries)
    {
        *numEntries = nEntries;
    }
    int getNumEntries()
    {
        return *numEntries;
    }
    void incNumEntries(int increment = 1)
    {
        if (*numEntries == maxEntries)
            throw runtime_error("Node full");
        else
            *numEntries += increment;
    }
    void decNumEntries(int decrement = 1)
    {
        if (*numEntries == 0)
            throw runtime_error("Node empty");
        else
            *numEntries -= decrement;
    }
    int nodeSearch(KeyType key){
        int i;
        for(i = 0 ;i < *numEntries; i++){
            if(keys[i] == key) return i;
            else if(keys[i] > key) return i;
        }
        return i;
    }
};

class PageManager
{
private:
    fstream * dataFile;
    int pageSize;
    PageID rootPageID,nPage;
public:
    PageManager(string fileName,int nPageSize){
        pageSize=nPageSize;
        dataFile = new fstream;
        dataFile->open(fileName.data(), fstream::binary | fstream::in | fstream::out);
        if(!dataFile->is_open())
        {
            dataFile->open(fileName.data(), fstream::binary | fstream::trunc | fstream::out);
            dataFile->close();
            delete dataFile;
            dataFile = new fstream;
            dataFile->open(fileName.data(), fstream::binary | fstream::in | fstream::out);
            dataFile->seekg(ios::beg);
            nPage=0;
            dataFile->write((char *)&nPage,sizeof(PageID));
            dataFile->seekg(ios::beg + sizeof(PageID));
            rootPageID=0;
            dataFile->write((char *)&rootPageID,sizeof(PageID));
        }else {
            dataFile->seekg(ios::beg);
            dataFile->read((char *)&nPage,sizeof(PageID));
            dataFile->seekg(ios::beg + sizeof(PageID));
            dataFile->read((char *)&rootPageID,sizeof(PageID));

        //cout<< "Arquivo Existe"<< endl;
    }
    };
    ~PageManager(){
        dataFile->close();
        delete dataFile;
    };
    Page * getNewPage(){
        dataFile->seekg(ios::beg);
        dataFile->read((char *) &nPage,sizeof(PageID));
        nPage ++;
        dataFile->seekg(ios::beg);
        dataFile->write((char *) &nPage,sizeof(PageID));
        Page * newpage;
        newpage = new Page(pageSize);
        newpage->setId(nPage);
        return newpage;
    };
    Page * getPage(PageID pageId){
        PageID pageId_;
        Page * page_;
        page_ = new Page(pageSize);
        char memblock[pageSize];
        dataFile->seekg( (ios::beg) + (sizeof(PageID) + sizeof(PageID)) + ( (pageId-1)*(sizeof(PageID) + (sizeof(char)*pageSize)) ) );
        dataFile->read((char *) &pageId_, sizeof(PageID));
        if(pageId==pageId_){
        page_->setId(pageId_);
        dataFile->seekg( (ios::beg) + (sizeof(PageID) + sizeof(PageID)) + ( (pageId-1)*(sizeof(PageID) + (sizeof(char)*pageSize)) ) + (sizeof(PageID)) );
        dataFile->read((char *)memblock,pageSize);
        memcpy(page_->getData(),memblock,pageSize);
        return page_;}

    };
    void writePage(Page * page){
        PageID page_;
        page_ = page->getId();
        dataFile->seekg( (ios::beg) + (sizeof(PageID) + sizeof(PageID)) + ( (page_-1)*(sizeof(PageID) + (sizeof(char)*pageSize)) ) );
        dataFile->write((char *) &page_,sizeof(PageID));
        dataFile->seekg( (ios::beg) + (sizeof(PageID) + sizeof(PageID)) + ( (page_-1)*(sizeof(PageID) + (sizeof(char)*pageSize)) ) + (sizeof(PageID)) );
        dataFile->write((char *)page->getData(),pageSize);
    };
    void releasePage(Page * page){delete page;};
    void deletePage(Page * page){
        PageID page_;
        PageID Id = -1;
        page_ = page->getId();
        dataFile->seekg( (ios::beg) + (sizeof(PageID) + sizeof(PageID)) + ( (page_-1)*(sizeof(PageID) + (sizeof(char)*pageSize)) ) );
        dataFile->write((char *) &Id,sizeof(PageID));
    };
    PageID getRootPageId(){
        PageID raiz;
        dataFile->seekg(ios::beg + sizeof(PageID));
        dataFile->read((char *) &raiz, sizeof(PageID));
        return raiz;
    };
    void setRootPageId(PageID newRootPageID){
        dataFile->seekg(ios::beg + sizeof(PageID));
        dataFile->write((char *) &newRootPageID, sizeof(PageID));
    };
};

template < class KeyType, int OBJECT_SIZE >
class BTree
{

private:
    enum status{InsertNotComplete, Success, DuplicateKey, Underflow, NotFound};

    PageManager* pageManager;

    status ins(PageID pageId, KeyType key, char * obj, KeyType & keyUp, char ** objUp, PageID & pointerUp)
    {
        int i, j;
        status code;

        if (pageId == 0) {
            keyUp = key;
            *objUp = obj;
            pointerUp = 0;
            return InsertNotComplete;
        }

        Page * page = pageManager->getPage(pageId);
        BNode<KeyType,OBJECT_SIZE> node(page, false);
        i = node.nodeSearch(key);

        if (i < node.getNumEntries() && key == node.getKey(i))
            return DuplicateKey;

        code = ins(node.getPointer(i), key, obj, keyUp, objUp, pointerUp);
        if (code != InsertNotComplete)
            return code;

        // VERIFIQUE SE AINDA CABE NO N� CORRENTE
        if (node.getNumEntries() < node.getMaxEntries()) {
            for (j = node.getNumEntries(); j > i; j--) {
                node.setKey(j, node.getKey(j - 1));
                node.setObject(j, node.getObject(j - 1));
                node.setPointer(j + 1, node.getPointer(j));
            }
            node.setKey(i, keyUp);
            node.setObject(i, *objUp);
            node.setPointer(i + 1, pointerUp);
            node.incNumEntries();

//            for(int i=0; i<node.getNumEntries(); i++)
//                cout << node.getKey(i) << ", "; //////////////////////////////////////////////////////////////////////////////
//            cout << endl;

            pageManager->writePage(page);
            pageManager->releasePage(page);

            return Success;
        }

        //O N� CORRENTE EST� CHEIO E SOFRER� SPLIT.
        Page * newPage = pageManager->getNewPage();
        BNode<KeyType,OBJECT_SIZE> newNode(newPage, true);

        KeyType lastKey;
        char * lastObj;
        PageID lastPointer;

        if (i == node.getNumEntries()) { //O OVERFLOW (lastKey, lastObj, lastPointer) EST� NA CHAVE/PONTEIRO QUE SUBIRAM
            lastKey = keyUp;
            lastObj = *objUp;
            lastPointer = pointerUp;
        }
        else { //O OVERFLOW N�O EST� NA CHAVE/PONTEIRO QUE SUBIRAM, DESLOQUE AS ENTRADAS DO N�
            lastKey = node.getKey(node.getNumEntries() - 1);
            lastObj = node.getObject(node.getNumEntries() - 1);
            lastPointer = node.getPointer(node.getNumEntries());
            for (j = node.getMaxEntries() - 1; j > i; j--) {
                node.setKey(j, node.getKey(j - 1));
                node.setObject(j, node.getObject(j - 1));
                node.setPointer(j + 1, node.getPointer(j));
            }
            node.setKey(i, keyUp);
            node.setObject(i, *objUp);
            node.setPointer(i + 1, pointerUp);
        }

        int median = node.getMaxEntries() / 2; // IDENTIFIQUE O �NDICE DO ELEMENTO CENTRAL QUE IR� "SUBIR"
        keyUp = node.getKey(median); // ELEMENTO CENTRAL
//        cout << "Max: " << node.getMaxEntries() << endl;
//        cout << "Up: " << keyUp << endl; /////////////////////////////////////////////////////////////////////////////

        *objUp = node.getObject(median); // ELEMENTO CENTRAL
        pointerUp = newPage->getId(); // RETORNE O "PONTEIRO" PARA O NOVO N�

        // MOVA AS ENTRADAS A PARTIR DE median + 1 DO N� ATUAL (LEFT) PARA O NOVO N� (RIGHT), E ACRESCENTE O OVERFLOW NO NOVO N�
        newNode.setNumEntries(node.getNumEntries() - median); //p[median+1],k[median+1],...,k[numEntries()-1],p[numEntries()],lastKey,lastPointer VAO PARA O NOVO N� (RIGHT)
        for (j = 0; j < newNode.getNumEntries(); j++) {
            newNode.setPointer(j, node.getPointer(j + median + 1));
            if (j < newNode.getNumEntries() - 1) {
                newNode.setKey(j, node.getKey(j + median + 1));
                newNode.setObject(j, node.getObject(j + median + 1));
            }
            else {
                newNode.setKey(j, lastKey);
                newNode.setObject(j, lastObj);
            }
        }
        newNode.setPointer(newNode.getNumEntries(), lastPointer);

        // "REMOVER" DO N� ATUAL (LEFT) AS ENTRADAS QUE FORAM PARA O NOVO N� (RIGHT)
        node.setNumEntries(median); // p[0],k[0],p[1],...,k[median-1],p[median] CONTINUAM NESTE N� (LEFT)

//        for(int i=0; i<node.getNumEntries(); i++)
//            cout << node.getKey(i) << ", "; //////////////////////////////////////////////////////////////////////////////
//        cout << endl;

        // ATUALIZE OS N�S (ANTIGO (LEFT) E NOVO (RIGHT)) EM DISCO
        pageManager->writePage(page);
        pageManager->releasePage(page);
        pageManager->writePage(newPage);
        pageManager->releasePage(newPage);

        return InsertNotComplete;
    };
    status rem(PageID pageId, KeyType key)
    {
        if (pageId == 0)
            return NotFound; // (SUB)ARVORE VAZIA

        int i, j;
        status code;
        Page * page = pageManager->getPage(pageId);
        BNode<KeyType,OBJECT_SIZE> node(page, false);

        i = node.nodeSearch(key);

        // O n� corrente � folha?
        if (node.getPointer(0) == 0) {
            // A chave a ser removida est� em node?
            if (i >= node.getNumEntries() || key != node.getKey(i)) {
                // N�o, chave n�o encontrada
                pageManager->releasePage(page);
                return NotFound;
            }

            // Sim, a chave a ser removida � node.getKey(i)
            for (j = i; j < node.getNumEntries() - 1; j++) {
                node.setKey(j, node.getKey(j + 1));
                node.setObject(j, node.getObject(j + 1));
                node.setPointer(j + 1, node.getPointer(j + 2));
            }
            node.decNumEntries();

            status ret;
            // O n� corrente � a raiz?
            if (pageId == pageManager->getRootPageId()) {
                if (node.getNumEntries() >= 1)
                    ret = Success;
                else
                    ret = Underflow;
            }
            else {
                if (node.getNumEntries() >= (int) (node.getMaxEntries() / 2))
                    ret = Success;
                else
                    ret = Underflow;
            }

            pageManager->writePage(page);
            pageManager->releasePage(page);
            return ret;
        }

        // O n� corrente � um n� interno
        // A CHAVE A SER REMOVIDA est� em node?
        if (i < node.getNumEntries() && key == node.getKey(i)) {
            // Encontre a maior chave menor que key (filho mais � direita da sub�rvore � esquerda de key[i])
            PageID nextPageId = node.getPointer(i);
            Page * pageFilho;
            BNode<KeyType,OBJECT_SIZE> * nodeFilho;

            for (;;) {
                pageFilho = pageManager->getPage(nextPageId);
                nodeFilho = new BNode<KeyType,OBJECT_SIZE>(pageFilho, false);

                // Encontrou a folha mais � direita?
                if (nodeFilho->getPointer(0) == 0) {
                    break;
                }
                else {
                    // Visite o n� mais � direita de nodeFilho
                    nextPageId = nodeFilho->getPointer(nodeFilho->getNumEntries());
                    pageManager->releasePage(pageFilho);
                    delete nodeFilho;
                }
            }//end for

            // TROQUE A CHAVE A SER REMOVIDA DO N� INTERNO PELA MAIOR CHAVE DE nodeFilho
            KeyType tmpKey;
            char * tmpObj;

            tmpKey = node.getKey(i);
            tmpObj = node.getObject(i);

            node.setKey(i, nodeFilho->getKey(nodeFilho->getNumEntries() - 1));
            node.setObject(i, nodeFilho->getObject(nodeFilho->getNumEntries() - 1));
            nodeFilho->setKey(nodeFilho->getNumEntries() - 1, tmpKey);
            pageManager->writePage(page);
            pageManager->writePage(pageFilho);
            pageManager->releasePage(pageFilho);
            delete nodeFilho;
        }

        // Visite o pr�ximo filho
        code = rem(node.getPointer(i), key);

        if (code != Underflow) {
            pageManager->releasePage(page);
            return code;
        }

        // O N� APONTADO POR node.getPointer(i) SOFREU UM UNDERFLOW
        Page * underflowPage = pageManager->getPage(node.getPointer(i));
        BNode<KeyType,OBJECT_SIZE> underflowNode(underflowPage, false);

        // underflowNode tem irm�o � esquerda?
        if (i > 0) {
            Page * leftBrotherPage = pageManager->getPage(node.getPointer(i - 1));
            BNode<KeyType,OBJECT_SIZE> leftBrotherNode(leftBrotherPage, false);

            // leftBrotherNode pode emprestar para underflowNode?
            if (leftBrotherNode.getNumEntries() > (int) (leftBrotherNode.getMaxEntries() / 2)) {

                // libere espa�o em underflowNode para a entrada emprestada, deslocando as entradas existentes para a direita
                underflowNode.setPointer(underflowNode.getNumEntries() + 1, node.getPointer(underflowNode.getNumEntries()));
                for (j = underflowNode.getNumEntries(); j > 0; j--) {
                    underflowNode.setKey(j, node.getKey(j - 1));
                    underflowNode.setObject(j, node.getObject(j - 1));
                    underflowNode.setPointer(j, node.getPointer(j - 1));
                }

                // des�a node.getKey(i - 1) para underflowNode
                underflowNode.setKey(0, node.getKey(i - 1));
                underflowNode.setObject(0, node.getObject(i - 1));

                // coloque a sub�rvore mais � direita de leftBrotherNode como a sub�rvore mais � esquerda de underflowNode
                underflowNode.setPointer(0, leftBrotherNode.getPointer(leftBrotherNode.getNumEntries()));

                // suba leftBrotherNode.getKey(leftBrotherNode.getNumEntries() - 1) para node
                node.setKey(i - 1, leftBrotherNode.getKey(leftBrotherNode.getNumEntries() - 1));
                node.setObject(i - 1, leftBrotherNode.getObject(leftBrotherNode.getNumEntries() - 1));

                // atualize as quantidades de entradas de leftBrotherNode e underflowNode
                leftBrotherNode.decNumEntries();
                underflowNode.incNumEntries();

                // escreva em disco os n�s alterados e libere os recursos
                pageManager->writePage(leftBrotherPage);
                pageManager->releasePage(leftBrotherPage);
                pageManager->writePage(underflowPage);
                pageManager->releasePage(underflowPage);
                pageManager->writePage(page);
                pageManager->releasePage(page);
                return Success;
            }//end if (leftBrotherNode pode emprestar)

            // se underflowNode n�o tiver irm�o � direita, underflowNode e leftBrotherNode sofrer�o unsplit
            if (i >= node.getNumEntries()) {
                // des�a node.getKey(i - 1) para leftBrotherNode
                leftBrotherNode.setKey(leftBrotherNode.getNumEntries(), node.getKey(i - 1));
                leftBrotherNode.setObject(leftBrotherNode.getNumEntries(), node.getObject(i - 1));

                // desloque as entradas restantes em node para a esquerda
                for (j = 0; j < node.getNumEntries(); j++) {
                    node.setKey(j, node.getKey(j + 1));
                    node.setObject(j, node.getObject(j + 1));
                    node.setPointer(j, node.getPointer(j + 1));
                }
                node.setPointer(node.getNumEntries(), node.getNumEntries() + 1);

                // atualize a quantidade de entradas de node
                node.decNumEntries();

                // copie as entradas de underflowNode para leftBrotherNode
                for (j = 0; j < underflowNode.getNumEntries(); j++) {
                    leftBrotherNode.setKey(j + leftBrotherNode.getNumEntries() + 1, underflowNode.getKey(j));
                    leftBrotherNode.setObject(j + leftBrotherNode.getNumEntries() + 1, underflowNode.getObject(j));
                    leftBrotherNode.setPointer(j + leftBrotherNode.getNumEntries() + 1, underflowNode.getPointer(j));
                }
                leftBrotherNode.setPointer(underflowNode.getNumEntries() + leftBrotherNode.getNumEntries() + 1, underflowNode.getPointer(underflowNode.getNumEntries()));

                // atualize a quantidade de entradas de leftBrotherNode
                leftBrotherNode.setNumEntries(underflowNode.getNumEntries() + leftBrotherNode.getNumEntries() + 1);

                // remova o n� underflowNode da �rvore
                pageManager->deletePage(underflowPage);

                status ret;
                // O n� corrente � a raiz?
                if (pageId == pageManager->getRootPageId()) {
                    if (node.getNumEntries() >= 1)
                        ret = Success;
                    else
                        ret = Underflow;
                }
                else {
                    if (node.getNumEntries() >= (int) (node.getMaxEntries() / 2))
                        ret = Success;
                    else
                        ret = Underflow;
                }

                // escreva em disco os n�s alterados e libere os recursos
                pageManager->writePage(leftBrotherPage);
                pageManager->releasePage(leftBrotherPage);
                pageManager->writePage(page);
                pageManager->releasePage(page);

                return ret;

            }//end unsplit underflowNode e leftBrotherNode

        }//end if (tem irm�o � esquerda)

        // carregue irm�o � direita de underflowNode
        Page * rightBrotherPage = pageManager->getPage(node.getPointer(i + 1));
        BNode<KeyType,OBJECT_SIZE> rightBrotherNode(rightBrotherPage, false); //left

        // rightBrotherNode pode emprestar para underflowNode?
        if (rightBrotherNode.getNumEntries() > (int) (rightBrotherNode.getMaxEntries() / 2)) {

            // des�a node.getKey(i) para underflowNode
            underflowNode.setKey(underflowNode.getNumEntries(), node.getKey(i));
            underflowNode.setObject(underflowNode.getNumEntries(), node.getObject(i));

            // coloque a sub�rvore mais � esquerda de rightBrotherNode como a sub�rvore mais � direita de underflowNode
            underflowNode.setPointer(underflowNode.getNumEntries() + 1, rightBrotherNode.getPointer(0));

            // suba rightBrotherNode.getKey(0) para node
            node.setKey(i, rightBrotherNode.getKey(0));
            node.setObject(i, rightBrotherNode.getObject(0));

            // desloque as entradas restantes em rightBrotherNode para a esquerda
            for (j = 0; j < rightBrotherNode.getNumEntries(); j++) {
                rightBrotherNode.setKey(j, rightBrotherNode.getKey(j + 1));
                rightBrotherNode.setObject(j, rightBrotherNode.getObject(j + 1));
                rightBrotherNode.setPointer(j, rightBrotherNode.getPointer(j + 1));
            }
            rightBrotherNode.setPointer(rightBrotherNode.getNumEntries(), rightBrotherNode.getNumEntries() + 1);

            // atualize as quantidades de entradas de rightBrotherNode e underflowNode
            rightBrotherNode.decNumEntries();
            underflowNode.incNumEntries();

            // escreva em disco os n�s alterados e libere os recursos
            pageManager->writePage(rightBrotherPage);
            pageManager->releasePage(rightBrotherPage);
            pageManager->writePage(underflowPage);
            pageManager->releasePage(underflowPage);
            pageManager->writePage(page);
            pageManager->releasePage(page);
            return Success;
        }//end if (rightBrotherNode pode emprestar)

        // underflowNode e rightNode sofrer�o unsplit
        // des�a node.getKey(i) para underflowNode
        underflowNode.setKey(underflowNode.getNumEntries(), node.getKey(i));
        underflowNode.setObject(underflowNode.getNumEntries(), node.getObject(i));

        // desloque as entradas restantes em node para a esquerda
        for (j = 0; j < node.getNumEntries(); j++) {
            node.setKey(j, node.getKey(j + 1));
            node.setObject(j, node.getObject(j + 1));
            node.setPointer(j, node.getPointer(j + 1));
        }
        node.setPointer(node.getNumEntries(), node.getNumEntries() + 1);

        // atualize a quantidade de entradas de node
        node.decNumEntries();

        // copie as entradas de rightBrotherNode para underflowNode
        for (j = 0; j < rightBrotherNode.getNumEntries(); j++) {
            underflowNode.setKey(j + underflowNode.getNumEntries() + 1, rightBrotherNode.getKey(j));
            underflowNode.setObject(j + underflowNode.getNumEntries() + 1, rightBrotherNode.getObject(j));
            underflowNode.setPointer(j + underflowNode.getNumEntries() + 1, rightBrotherNode.getPointer(j));
        }
        underflowNode.setPointer(rightBrotherNode.getNumEntries() + underflowNode.getNumEntries() + 1, rightBrotherNode.getPointer(rightBrotherNode.getNumEntries()));

        // atualize a quantidade de entradas de underflowNode
        underflowNode.setNumEntries(rightBrotherNode.getNumEntries() + underflowNode.getNumEntries() + 1);

        // remova o n� rightBrotherNode da �rvore
        pageManager->deletePage(rightBrotherPage);

        status ret;
        // O n� corrente � a raiz?
        if (pageId == pageManager->getRootPageId()) {
            if (node.getNumEntries() >= 1)
                ret = Success;
            else
                ret = Underflow;
        }
        else {
            if (node.getNumEntries() >= (int) (node.getMaxEntries() / 2))
                ret = Success;
            else
                ret = Underflow;
        }

        // escreva em disco os n�s alterados e libere os recursos
        pageManager->writePage(underflowPage);
        pageManager->releasePage(underflowPage);
        pageManager->writePage(page);
        pageManager->releasePage(page);

        return ret;
    };

public:
    BTree(PageManager * pManager) {
        pageManager = pManager;
    }
    ~BTree(){};

    void insert(KeyType key, char * obj)
    {
        PageID pointerUp;
        KeyType keyUp;
        char * objUp;

        status code = ins(pageManager->getRootPageId(), key, obj, keyUp, &objUp, pointerUp);

        if (code == DuplicateKey) {
            cout << "Duplicate key ignored.\n";
            return;
        }

        if (code == InsertNotComplete) {
            Page * newPage;
            newPage = pageManager->getNewPage();
            BNode<KeyType,OBJECT_SIZE> newRootNode(newPage, true);

            newRootNode.setPointer(0, pageManager->getRootPageId());
            newRootNode.setKey(0, keyUp);
            newRootNode.setObject(0, objUp);
            newRootNode.incNumEntries();
            newRootNode.setPointer(1, pointerUp);

            pageManager->setRootPageId(newPage->getId());
            pageManager->writePage(newPage);
            pageManager->releasePage(newPage);
        }

        cout << "Success.\n";

    };
    void remove(KeyType key)
    {
        status code = rem(pageManager->getRootPageId(), key);

        if (code == NotFound) {
            cout << "Not found.\n";
            return;
        }

        if (code == Underflow) {
            Page * page;
            page = pageManager->getPage(pageManager->getRootPageId());
            BNode<KeyType,OBJECT_SIZE> oldRootNode(page, false);
            pageManager->setRootPageId(oldRootNode.getPointer(0)); //Nova raiz � filha de oldRootNode
            pageManager->deletePage(page);
        }

        cout << "Success.\n";
    };
    bool search(KeyType key){
        KeyType Key_1 = 0,Key_2 = 0;
        PageID root;
        PageID Page_ = 1;
        root = pageManager->getRootPageId();
        if(root != 0){
            Page * pageAux;
            pageAux = pageManager->getPage(root);
            BNode<KeyType,OBJECT_SIZE> * Aux;
            Aux = new BNode<KeyType,OBJECT_SIZE>(pageAux,false);
            Key_1 = Aux->nodeSearch(key);
            Key_2 = Aux->getKey(Key_1);
            if(Key_2 == key){
                delete Aux;
                pageManager->releasePage(pageAux);
                cout <<"ENCONTRADO"<<endl;
                return true;
            }else{
                while(Page_!=0){
                    Page_ = Aux->getPointer(Key_1);
                    delete Aux;
                    if(Page_!=0){
                        Aux = new BNode<KeyType,OBJECT_SIZE>(pageManager->getPage(Page_),false);
                        Key_1 = Aux->nodeSearch(key);
                        Key_2 = Aux->getKey(Key_1);
                        if(key == Key_2){
                            delete Aux;
                            pageManager->releasePage(pageAux);
                            cout << "ENCONTRADO" << endl;
                            return true;
                        }}
                }if(Page_ == 0){
                    pageManager->releasePage(pageAux);
                    cout << "NAO FOI ENCONTRADO" << endl;
                    return false;
                }}
        }else{
            cout << "A ARVORE ESTA VAZIA"<<endl;
            return false;
        }
    };
};

int main(void)
{
    PageManager *Noel;
    Noel = new PageManager("Presentes",512);
    BTree<int,20> ArvoredeNatal(Noel);

    long long int *presente = new long long int(100);
    srand( time(NULL) );
    for(int i = 0; i < 3000; i++){
       long long int randonKey = rand() %100000000;
        *presente = rand() %200000;
        cout << "Key: "<<randonKey<<" Presente: "<<*presente<<" Status: ";
        ArvoredeNatal.insert(randonKey,(char*)presente);
    }
    *presente = rand() %200000;
    cout << "Key: "<<"6"<<" Presente: "<<*presente<<" Status: ";
    ArvoredeNatal.insert(6,(char*)presente);
    *presente = rand() %200000;
    cout << "Key: "<<"1263"<<" Presente: "<<*presente<<" Status: ";
    ArvoredeNatal.insert(1263,(char*)presente);
    delete Noel;

    PageManager *Rudolph;
    Rudolph = new PageManager("Presentes",512);
    BTree<int,20> ArvoreComPresentes(Rudolph);

    for(int i = 0; i < 10; i++){
        long long int randonKey = rand() %10000;
        cout<<"KEY: "<<randonKey<<" - ";
        ArvoreComPresentes.search(randonKey);
    }
    cout<<"KEY: "<<"6"<<" - ";
    ArvoreComPresentes.search(6);
    cout<<"KEY: "<<"1263"<<" - ";
    ArvoreComPresentes.search(1263);

    return 0;
}
